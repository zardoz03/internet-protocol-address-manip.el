;;; subnet.el --- Description -*- lexical-binding: t; -*-
;; This file is not part of GNU Emacs.
;;; Commentary:
;;  Description
;;; Code:

;;; FIXME: FIX ENDIAN ISSUES AND MAKE ALL VALUES BIG ENDIAN

;;(push (file-name-directory buffer-file-name) load-path)
;;(setf load-path (-uniq load-path))

(require 'cl-lib)
(require 'dash)
(require 'seq)
;(require 'big-endian)

(defmacro comment (&rest body)
  "Ignore all input of BODY when compiling."
  nil)

(defun ipv4-new (seq)
  "Assemble octets in SEQ as an IPv4 Address."
  (let ((seq (reverse seq)))
    (-> (seq-elt seq 0)
        (ash 8) (logior (seq-elt seq 1))
        (ash 8) (logior (seq-elt seq 2))
        (ash 8) (logior (seq-elt seq 3)))))

(comment
 (->> (ipv4-new [192 43 244 18])
      (format-ip-ipv4))
                                        ;=> "192.43.244.18"
 )

;; GOOGLE ENCAPSULATION DOT1Q

(defalias 'ipv4-netmask->wildcardmask #'lognot
  "Convert subnetwork mask IP to a wildcard mask.

\(fn NETMASK)")
(defalias 'ipv4-wildcardmask->netmask #'lognot
  "Convert wildcard mask to subnetwork mask IP.

\(fn WILDCARDMASK)")

(comment
 (equal '(0 0 0 255)
        (->> (ipv4-new [255 255 255 0])
             (ipv4-netmask->wildcardmask)
             (ipv4-octets)))
 ;;=> t
 (equal '(0 0 0 3)
        (->> (ipv4-new [255 255 255 252])
             (ipv4-netmask->wildcardmask)
             (ipv4-octets)))
 ;;=> t
 (equal '(255 255 255 252)
        ;; same procedure, but semantically different
        (->> (ipv4-new [0 0 0 3])
             (ipv4-wildcardmask->netmask)
             (ipv4-octets)))
 ;;=> t
 )

(defalias 'ipv4-network-ip #'logand
  "Return network IP from IP and subnetwork mask IP.

\(fn IPV4 NETMASK)")

(defun ipv4-netmask-length (netmask)
  "Return number of continuous zeros in a subnetwork mask."
  ;; `logcount' is not continuous by default
  (logcount netmask))

(comment
 (equal '(192 168 1 0)
        (->> (ipv4-new '(192 168 1 3))
             (ipv4-network-ip (ipv4-subnet-mask-from :class :c))
             (ipv4-octets)))
 ;;=> t
 )

(defvar ipv4-class2netmask-length
  '(;;∇  general purpose
    (:a . 8)
    (:b . 16)
    (:c . 24)
    ;; theres probably a way to make a fake-netmask that encodes the :d,:e
    ;; values, but it would be easier to have it in a separate table.
    (:d . :n/a) ;; multicast
    (:e . :n/a) ;; research/experiments
    )
  "Map of IPv4 class designator and length of its netword mask.")

(defun ipv4-subnet-mask-from (how data)
  "Get a subnet mask from DATA and HOW to generate it, either bitlength or classed.

HOW  := classful | classless.
DATA := :kw | [0-9].

# Examples.
: (= (ipv4-subnet-mask-from :class :c) (ipv4-subnet-mask-from :/ 24)) ;=> t

: (format-ip-ipv4 (ipv4-subnet-mask-from :/ 25)) ;=> 255.255.255.129

: (format-ip-ipv4 (ipv4-subnet-mask-from :class :a)) ;=> 255.0.0.0

: (ipv4-subnet-mask-from :rfc791 :c) ;=> class c subnet mask"
  (cl-case how
    ((or :bitlength :bit-length :bit :length :size :classless :cidr :vlsm
         :/
         ;; 151(8|9)|4632|950
         ;; https://datatracker.ietf.org/doc/html/rfc1518
         ;; FIXME: ???, read thru docs to see if its reasonable or not
         :rfc-1518 :rfc-1519 :rfc-4632 :rfc-950
         :rfc1518 :rfc1519 :rfc4632 :rfc950)

     (unless (< data 33)
       (user-error
        "Use `ipv6-subnet-mask-from' instead! Data is outside address-space magnitude."))

     (named-let lp ((n 0) (a 0))
       (cond
        ;; ipv4 is 32bit, don't call this a magic number or so help me God
        ((= n data)
         ;; FIXME: fix problem w/:bit 25
         a)
        (t
         (lp (1+ n)
             (-> a (ash 1) (logior 1)))))))

    ((or :class :classful
         ;; https://datatracker.ietf.org/doc/html/rfc791
         :rfc-791 :rfc791)
     (let* ((class
             (let ((sym (->> (if (symbolp data)
                                 (symbol-name data)
                               data)
                             (downcase))))
               (cond
                ((keywordp data) (intern sym))
                ((symbolp  data) (intern (concat ":" sym)))
                ((stringp  data) (intern (concat ":" sym)))
                (t (user-error "Unknown Descriptor due to wrong obj-type! data:=%s"
                               data)))))

            (length (alist-get class ipv4-class2netmask-length)))

       (if (or (eq :n/a length) (not length))
           (user-error "Unknown or Inapplicable IPV4 network class! %s->%s"
                       class length)
         (ipv4-subnet-mask-from :length length))))

    (t (user-error "Unknown method to return a subnet mask!"))))

(comment
 (equal '(255 255 255 0)
        (ipv4-octets (ipv4-subnet-mask-from :classful :c)))
 ;;=> t
 (= (ipv4-subnet-mask-from :classful  :c)
    (ipv4-subnet-mask-from :bitlength 24))
 ;;=> t
 (->> (logand (ipv4-subnet-mask-from :bit 25)
              (ipv4-new [192 168 1 127]))
      (ipv4-octets))
 ;;=> (192 168 1 129))
 )

(defun ipv4-octet-elt (ip n)
  "Return octet N from IP."
  (when (>= n 4)
    (user-error "Out of bounds!"))
  (let ((i -1)
        ret)
    (while (< i n)
      (setf ret (logand ip #xFF)) ; 8bit mask
      (setf ip (ash ip -8))       ; shift right by 8
      (cl-incf i))
    ret))

(defun octet-mask (n)
  "Return a mask for the Nth octet in IPv4 Ip.

: (octet-mask 2) => #xFFFF"
  (let ((i 0)
        (a 0))
    (while (< i n)
      (setf a (-> a (ash 8) (logior #xFF)))
      ;; move left by 8 and add 8 bit mask
      (cl-incf i))
    a))

(defun poke-ipv4-octet (ip n value)
  "Return new IP with the Nth octet replaced with VALUE."
  (let* ((mask      (octet-mask n))
         ;; values we intend to keep that get ignored by first mask
         (rest      (logand ip (lognot (octet-mask (1+ n)))))
         ;; so it ORs correctly with the IP
         (new-octet (ash value (* n 8))))
    (-> ip
        (logand mask)
        (logior new-octet)
        (logior rest))))

;; copied from $EMACS_SRC/lisp/emacs-lisp/gv.el's substring
;; and i have no idea how it works at all, beyond being a macro that uses
;; `poke-ipv4-octet' instead of `ipv4-octet-elt' in `setf' calls
(gv-define-expander ipv4-octet-elt
  (lambda (do ip n)
    (gv-letplace (getter setter) ip
      (funcall do `(poke-ipv4-octet ,getter ,n ,setter)
               (lambda (v)
                 (macroexp-let2 nil v v
                   `(progn
                      ,(funcall setter `(poke-ipv4-octet ,getter ,n ,v))
                      ,v)))))))

(comment
 (equal '(192 168 2 103)
        (let* ((a (ipv4-new [192 168 1 103])))
          (setf (ipv4-octet-elt a 2) 2)
          (ipv4-octets a)))
 ;=> t
 (equal '(192 168 1 2)
        (ipv4-octets (poke-ipv4-octet (ipv4-new '(192 168 1 103))
                                      3 2)))
 ;=> t
 )

(defun ipv4-subnet-address-space-size (netmask)
  "Return the size of the subnet address space specified by NETMASK."
  ;; 2^32 is the entire address space, thus constrict by length of netmask
  (expt 2 (- 32 (ipv4-netmask-length netmask))))

(defun ipv4-subnet-address-space (ip netmask)
  (let* ((netip (ipv4-network-ip ip netmask))
         (highest-ip (purecopy netip)))
    (setf (ipv4-octet-elt highest-ip 3)
          (+ (ipv4-octet-elt highest-ip 3)
             (1- (ipv4-subnet-address-space-size netmask))))
    (list netip highest-ip)))

(defun ipv4-subnet-available-networks-count (netmask)
  "Number of available networks for NETMASK.
Relative to nearest relevant IPv4 class to the NETMASK."
  (let ((netmask-length (ipv4-netmask-length netmask))
        ;; presume contiguous netmasks
        (nearest-class
         (->> ipv4-class2netmask-length
              (-map
               (lambda (x)
                 (cons (car x)
                       (if (numberp (cdr x))
                           (->> (cdr x)
                                (list netmask-len)
                                (-sort (lambda (x y)
                                         (and (numberp x) (numberp y)
                                              (> x y))))
                                (apply #'-)
                                (abs))))))
              (-filter #'cdr)
              (-sort (lambda (x y)
                       (and (numberp (cdr x))
                            (numberp (cdr y))
                            (< (cdr x) (cdr y)))))
              (car))))
    nearest-class))

(defun ipv4-subnet-available-network-ips (netmask)
  -1)

(comment
 (ipv4-subnet-address-space-size (ipv4-subnet-mask-from :bit 25))
 ;=> 128
 (ipv4-subnet-address-space-size (ipv4-subnet-mask-from :bit 24))
 ;=> 255
 )

(defun ipv4-octets (ip)
  "Return list of IPv4 octets from IP."
  (let ((i 0)
        ret)
    (while (< i 4)
      (push (logand ip #xFF) ret)  ; 8bit mask
      (setf ip (ash ip -8))        ; shift right by 8
      (cl-incf i))
    (cl-assert (= 4 (length ret)))
    (nreverse ret)))

(comment
 (equal '(1 2 3 4)
        (ipv4-octets (ipv4-new '(1 2 3 4))))
 ;=> t
 )

(defun format-ip-ipv4 (ip)
  "Return an IPv4 IP as its component octets as a string."
  (if (numberp ip) ip (user-error "Not a number!"))
  (apply #'format (cons "%d.%d.%d.%d" (ipv4-octets ip))))

(provide 'subnet)
;;; subnet.el ends here
