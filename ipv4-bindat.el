;;; ipv4-bindat.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2023 zardoz03
;;
;; Author: zardoz03 <zardoz030@gmail.com>
;; Maintainer: zardoz03 <zardoz030@gmail.com>
;; Package-Requires: ((emacs "28.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;
;;
;;; Code:
(require 'bindat)
(defmacro comment (&rest _body) nil)

(bindat-defmacro byte-be () '(uint 8 be))
(bindat-defmacro ipv4    () '(vec 4 byte-be))
(bindat-defmacro ipv4-subnetmask () '(or ipv4 int))

(comment
 (bindat-type
   (nm ipv4-subnetmask)
   (ip ipv4))
 ;; since ipv4-subnetmask is (OR IPV4 INT) then it should be equally
 => ((nm 25)
     (ip (192 168 1 129)))
 ;; aswell as
 => ((nm (255 255 255 128))
     (ip (192 168 1   129))))


(provide 'ipv4-bindat)
;;; ipv4-bindat.el ends here
