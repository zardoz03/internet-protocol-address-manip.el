;;; big-endian.el --- BE manip. support on LE -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2023 zardoz03
;;
;; Author: zardoz03 <zardoz030@gmail.com>
;; Maintainer: zardoz03 <zardoz030@gmail.com>
;; Keywords: convenience data extensions hardware lisp tools networking
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;; Big Endian <-> Little Endian Manipulation for Emacs.
;;
;; You are free to rename the file to little-endian.el if you are on a
;; big-endian machine :D
;;
;; only tested on emacs29 as of writing.
;;
;;; Code:


(provide 'big-endian)
;;; big-endian.el ends here
